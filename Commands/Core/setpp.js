const fs = require("fs");
const Jimp = require("jimp");
require("../../Core.js");

module.exports = {
  name: "setpp",
  alias: ["setpnpp", "setpppn"],
  desc: "Set a Pn profile picture.",
  category: "Group",
  usage: `Tag an Image and type -setpp}`,
  react: "🍁",
  start: async (
    Miku,
    m,
    { text, prefix,  mentionByTag, pushName, mime, quoted }
  ) => {

    if (!/image/.test(mime))
      return Miku.sendMessage(
        m.from,
        {
          text: `Send/Reply Image With Caption ${
            prefix + "setpp"
          } PB ist geändert.`,
        },
        { quoted: m }
      );
    if (/webp/.test(mime))
      return Miku.sendMessage(
        m.from,
        {
          text: `Send/Reply Image With Caption ${
            prefix + "setpp"
          } PB ist geändert.`,
        },
        { quoted: m }
      );

      let quotedimage = await Miku.downloadAndSaveMediaMessage(quoted)
      var { preview } = await generatePP(quotedimage)   
      
      await Miku.query({
        tag: 'iq',
        attrs: {
            to: m.from,
            type:'set',
            xmlns: 'w:profile:picture'
        },
        content: [{
            tag: 'picture',
            attrs: { type: 'image' },
            content: preview
        }]
    })
    fs.unlinkSync(quotedimage)

    pppn = await Miku.profilePictureUrl(m.from, "image");

    Miku.sendMessage(
        m.from,
        { image: {url: pppn},caption: `\nProfilbild pn geändert*${pushName}* !` },
        { quoted: m }
      )
  },
};

async function generatePP(buffer) {
    const jimp = await Jimp.read(buffer)
    const min = jimp.getWidth()
    const max = jimp.getHeight()
    const cropped = jimp.crop(0, 0, min, max)
    return {
        img: await cropped.scaleToFit(720, 720).getBufferAsync(Jimp.MIME_JPEG),
        preview: await cropped.normalize().getBufferAsync(Jimp.MIME_JPEG)
    }
}
