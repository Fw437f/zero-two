const moment = require('moment-timezone')

module.exports = {
    name: "support",
    alias: ["issue"],
    desc: "Um eine support anfrage zu stellen",
    cool:3600,
    category: "Group",
    usage: `support <describe issue>`,
    react: "🍁",
    start: async (
      Miku,
      m,
      { text, prefix, isBotAdmin, isAdmin, pushName, metadata, args }
    ) => {
        if(!m.isGroup){
            if (!args[0]) return m.reply(`Bitte senden Sie eine Nachricht, um etwas zu melden oder eine Frage zu stellen!`);
            let userTag = m.sender.split("@")[0];
            let userMess = args.join(" ");
            let userName = pushName;

            try {
                userPfp = await Miku.profilePictureUrl(m.sender, "image");
              } catch (e) {
                userPfp = botImage3;
              }

            let supportMessage = `              *「 Support Erhalten 」*\n\n*👤 Berichtet von:* @${userTag}\n\n*📝 Nachricht:* ${userMess}\n\n*📅 Datum:* ${moment().tz('Asia/Kolkata').format('DD/MM/YYYY')}\n*⏰ Zeit:* ${moment().tz('Asia/Kolkata').format('hh:mm:ss A')}\n*🍁 Verwendete Character:* ${botName}\n\n*📌 Notiz: Dies ist eine automatisierte Nachricht, bitte antworten Sie nicht auf diese Nachricht, um eine Blockierung zu vermeiden.*`;
            m.reply(`Bericht wird gesendet...\n\nWenn es sich um Spam handelt, werden Sie möglicherweise *Blockiert* und *Gebannt*.`);
            
            let devs = [`4915212908434@s.whatsapp.net`]

            for (let i = 0; i < devs.length; i++) {
              await Miku.sendMessage(devs[i],{image: {url: userPfp}, caption: reportMessage,mentions: [m.sender],});
            }
        }
        else{
            if (!args[0]) return m.reply(`Bitte senden Sie eine Nachricht, um etwas zu melden oder eine Frage zu stellen!`);
            let userTag = m.sender.split("@")[0];
            let userMess = args.join(" ");
            let userName = pushName;
            let gcName = metadata.subject;

            try {
                ppgc = await Miku.profilePictureUrl(m.from, "image");
              } catch {
                ppgc = botImage3;
              }
              let reportMessage = `              *「 Support Erhalten 」*\n\n*👤 Berichtet von:* @${userTag}\n*🧩 Gruppen Name:* ${gcName}\n\n*📝 Nachricht:* ${userMess}\n\n*📅 Datum:* ${moment().tz('Asia/Kolkata').format('DD/MM/YYYY')}\n*⏰ Time:* ${moment().tz('Asia/Kolkata').format('hh:mm:ss A')}\n*🍁 Verwendete Character:* ${botName}\n\n*📌 Notiz: Dies ist eine automatisierte Nachricht, bitte antworten Sie nicht auf diese Nachricht, um eine Blockierung zu vermeiden.*`;
              m.reply(`Bericht wird gesendet...\n\nWenn es sich um Spam handelt, werden Sie möglicherweise *Blockiert* und *Gebannt*.`);

              let devs = [`4915212908434@s.whatsapp.net`]

              for (let i = 0; i < devs.length; i++) {
                await Miku.sendMessage(devs[i],{image: {url: ppgc}, caption: reportMessage,mentions: [m.sender],});
            }
        }
    }
}
